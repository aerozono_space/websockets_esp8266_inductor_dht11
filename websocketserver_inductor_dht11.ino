
#include <Arduino.h>

#include <ESP8266WiFi.h>

#include <WebSocketsServer.h> //requiere librería
#include <ESP8266WebServer.h>
#include <ESP8266mDNS.h>
#include <Hash.h>
#include <FS.h>

#include <Wire.h>
#include <Adafruit_BMP085.h> //librería

#define LED_RED     D4


#define USE_SERIAL Serial

#include <SimpleDHT.h>
int pinDHT11 = D3;
SimpleDHT11 dht11;
byte temperature = 0;
  byte humidity = 0;  

float temp_bmp;
int Pa;
float alt;

int contador;
unsigned long t;

const char *ssid = "SensoresAero";
const char *password = "";

 int half_revolutions = 0;

 int rpm = 0;

 unsigned long lastmillis = 0;


Adafruit_BMP085 bmp;

String buffer_fecha;
String msj;

ESP8266WebServer server(80);
WebSocketsServer webSocket = WebSocketsServer(81);

void webSocketEvent(uint8_t num, WStype_t type, uint8_t * payload, size_t length) {

    switch(type) {
        case WStype_DISCONNECTED:
            USE_SERIAL.printf("[%u] Disconnected!\n", num);
            break;
        case WStype_CONNECTED: {
            IPAddress ip = webSocket.remoteIP(num);
            USE_SERIAL.printf("[%u] Connected from %d.%d.%d.%d url: %s\n", num, ip[0], ip[1], ip[2], ip[3], payload);
    
            // send message to client
            
        }
            break;
        case WStype_TEXT:
         {   USE_SERIAL.printf("[%u] get Text: %s\n", num, payload);
          buffer_fecha= (char*)payload;

          msj=buffer_fecha+","+msj;
          File csv_log = SPIFFS.open("/log.csv", "a"); //Append

  csv_log.println(msj);
  if (csv_log.size()>2048000){SPIFFS.remove("/log.csv");}
 csv_log.close();
            }
            
            break;
    }
    }
    

void handleRoot()
{
  server.sendHeader("Location","/index.html",true);
  server.send(302, "text/plain","");
  }
void handleWebRequests()
{
        
  File file2 = SPIFFS.open("/index.html", "r");
        size_t sent2 = server.streamFile(file2, "text/html");
        file2.close();
        
  File file = SPIFFS.open("/Chart.min.js", "r");
        size_t sent = server.streamFile(file, "application/javascript");
        file.close();



        
        
        }

void setup() {
 // IPAddress myIP = WiFi.softAPIP();

    //USE_SERIAL.begin(921600);

    USE_SERIAL.begin(115200);

    USE_SERIAL.setDebugOutput(true);

    USE_SERIAL.println();
    USE_SERIAL.println();
    USE_SERIAL.println();

    for(uint8_t t = 4; t > 0; t--) {
        USE_SERIAL.printf("[SETUP] BOOT WAIT %d...\n", t);
        USE_SERIAL.flush();
        delay(1000);
    }

    pinMode(LED_RED, OUTPUT);

    digitalWrite(LED_RED, HIGH);
  delay(1000);
 IPAddress Ip(192, 168, 4, 1);
  IPAddress NMask(255, 255, 255, 0);

WiFi.setOutputPower(20.5);
  WiFi.softAPConfig(Ip, Ip, NMask);
  
    WiFi.softAP(ssid, password);
   

    // start webSocket server
    webSocket.begin();
    webSocket.onEvent(webSocketEvent);

  SPIFFS.begin();
Dir dir =SPIFFS.openDir("/");

    if(MDNS.begin("esp8266")) {
        USE_SERIAL.println("MDNS responder started");
    }

    // handle index

      server.on("/",handleRoot);
  //  server.onNotFound(handleWebRequests);
    
        // send index.html
       // server.send(200, "text/html", "<html><head><script>var connection = new WebSocket('ws://'+location.hostname+':81/', ['arduino']);connection.onopen = function () {  connection.send('Connect ' + new Date()); }; connection.onerror = function (error) {    console.log('WebSocket Error ', error);};connection.onmessage = function (e) {  console.log('AnalogRead: ', e.data); document.getElementById(\"ADCValue\").innerHTML =e.data;};function sendRGB() {  var r = parseInt(document.getElementById('r').value).toString();  if(r.length < 2) { r = '0' + r; }   var rgb= r;    console.log('RGB: ' + rgb); connection.send(rgb); }  </script></head><body>LED Control:<br/><br/>LED: <input id=\"r\" type=\"range\" min=\"0\" max=\"255\" step=\"1\" oninput=\"sendRGB();\" /> <div> Valor ADC0 : <span id=\"ADCValue\">0</span><br> </div></body></html>");

 server.begin();
  server.serveStatic("/Chart.min.js",SPIFFS, "/Chart.min.js");
  server.serveStatic("/utils.js",SPIFFS, "/utils.js");
 // server.serveStatic("/basic.html",SPIFFS, "/basic.html");  
 // server.serveStatic("/image.jpg",SPIFFS, "/image.jpg");  
  server.serveStatic("/index.html",SPIFFS, "/index.html");  
server.serveStatic("/log.csv",SPIFFS, "/log.csv");  

    

    // Add service to MDNS
    MDNS.addService("http", "tcp", 80);
    MDNS.addService("ws", "tcp", 81);

    digitalWrite(LED_RED, LOW);
    USE_SERIAL.println(WiFi.localIP());

 attachInterrupt(D8, rpm_fan, FALLING);
  bmp.begin();
}

unsigned long last_10sec = 0;
unsigned int counter = 0;





void loop() {
    t= millis();
    webSocket.loop();
    server.handleClient();
    
     
  /*  if((t - last_10sec) > 10 * 1000) {
        counter++;
        bool ping = (counter % 2);
        int i = webSocket.connectedClients(ping);
        USE_SERIAL.printf("%d Connected websocket clients ping: %d\n", i, ping);
        last_10sec = millis();
    } */
//contador++;
rpm_medir();

}


void rpm_medir(){
 if (millis() - lastmillis > 1000){
 detachInterrupt(D8);//Disable interrupt when calculating

 rpm = half_revolutions * 30; // Convert frecuency to RPM, note: this works for one interruption per full rotation. For two interrups per full rotation use half_revolutions * 30.
 rpm=rpm/4; 

 half_revolutions = 0; // Restart the RPM counter
 lastmillis = millis(); // Uptade lasmillis
 attachInterrupt(D8, rpm_fan, FALLING); //enable interrupt
 Pa=  bmp.readPressure();
 alt=bmp.readAltitude(bmp.readSealevelPressure());
 temp_bmp=bmp.readTemperature();
 dht11.read(pinDHT11, &temperature, &humidity, NULL);
 enviar_websocket();
 
  }}

   void rpm_fan(){
  half_revolutions++;
 }

void enviar_websocket(){
  if((t - last_10sec) > 5000){
  

  last_10sec = millis();

 msj= String(rpm)+","+String(temp_bmp)+","+String(humidity)+","+String(Pa)+","+String(alt);
 
 webSocket.broadcastTXT(msj);
 
    
     }
}
